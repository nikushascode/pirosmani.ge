<!doctype html>
<html lang="">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Mindfield</title>
<link rel="shortcut icon" href="<?php // echo base_url(); ?>themes/images/favicon.png">
<meta name="theme-color" content="#01579b">
<meta name="msapplication-navbutton-color" content="#01579b">
<meta name="apple-mobile-web-app-status-bar-style" content="#01579b">
<link rel="stylesheet" href="scripts/css/main.css">
<script type="text/javascript" src="scripts/js/main.js"></script>
</head>
<body>

<div class="loader">
	<div class="roller">
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
		<div></div>
	</div>
</div>

<div class="modal_overlay"></div>
<div class="modal widget medium">
	<div class="close">
		<em data-feather="x"></em>
	</div>
	<h1 class="modal_title"></h1>
	<div class="modal_content"></div>
</div>

<div class="sqema">
	<div class="sidebar_toggle light trans-all-4">
		<a href="javascript:void(0);" title="მენიუ">
			<span></span>
			<span></span>
			<span></span>
			<span></span>
			<span></span>
		</a>
	</div>
	<aside data-simplebar>
		<div class="logo">
			<svg viewBox="0 0 172 131">
				<g>
					<g>
						<g>
							<path d="M86.4,46V1.1"></path>
							<path d="M97.9,2.2l-11.5,9.7"></path>
							<path d="M125.9,23.1L106,34.5H86.4"></path>
							<path d="M130.6,34.5l-14.2-6"></path>
							<path d="M109,6.5l-5.5,9.5l2.5,18.5"></path>
							<path d="M118.3,13.6L103.5,16"></path>
							<path d="M63.7,7l5.1,9l17.6,7.1"></path>
							<path d="M74.2,2.2L68.8,16"></path>
						</g>
						<g>
							<path d="M86.4,46l38.9,22.5"></path>
							<path d="M118.6,78L116,63.1"></path>
							<path d="M86.5,91.7l0.1-22.9l9.8-17"></path>
							<path d="M74.3,90.1l12.2-9.3"></path>
							<path d="M109.3,85.4l-5.4-9.5l-17.3-7.1"></path>
							<path d="M98.5,89.9l5.4-14"></path>
							<path d="M131.5,45.9l-10.3-0.1l-15,11.7"></path>
							<path d="M130.4,57.4l-9.2-11.6"></path>
						</g>
						<g>
							<path d="M86.4,46L47.5,68.5"></path>
							<path d="M42.6,58l14.2,5.1"></path>
							<path d="M46.8,23.3l19.7,11.5l9.8,17"></path>
							<path d="M54.2,13.5l2,15.3"></path>
							<path d="M40.9,46.2l10.9,0.1l14.7-11.5"></path>
							<path d="M42.3,34.6l9.5,11.7"></path>
							<path d="M64,85.2l5.1-8.9l-2.6-18.8"></path>
							<path d="M54.5,78.5l14.6-2.2"></path>
						</g>
					</g>
				</g>
				<g>
					<g>
						<path d="M12.5,130.5v-11c0-3.2-2.6-5.7-5.7-5.7h0c-3.2,0-5.7,2.6-5.7,5.7v11"></path>
						<path d="M23.9,130.5v-11c0-3.2-2.6-5.7-5.7-5.7h0c-3.2,0-5.7,2.6-5.7,5.7v11"></path>
					</g>
					<g>
						<path d="M34.5,130.5L34.5,113.8"></path>
						<path d="M34.5,110.3L34.5,110.3"></path>
					</g>
					<path d="M45,130.5v-8.4c0-4.6,3.7-8.4,8.4-8.4s8.4,3.7,8.4,8.4v8.4"></path>
					<g>
						<path d="M69.69999999999999,122.2A8.4,8.4 0,1,1 86.5,122.2A8.4,8.4 0,1,1 69.69999999999999,122.2"></path>
						<path d="M86.4,130.5L86.4,105"></path>
					</g>
					<g>
						<path d="M94.4,113.4c0-4.6,3.7-8.4,8.4-8.4"></path>
						<path d="M94.4,130.5L94.4,112.9"></path>
						<path d="M94.4,122.2c0-4.6,3.7-8.4,8.4-8.4"></path>
					</g>
					<g>
						<path d="M111.1,130.5L111.1,113.8"></path>
						<path d="M111.1,110.3L111.1,110.3"></path>
					</g>
					<g>
						<path d="M133.7,128.6c-1.4,1.2-3.3,1.9-5.3,1.9c-4.6,0-8.4-3.7-8.4-8.4c0-4.6,3.7-8.4,8.4-8.4c4.6,0,8.4,3.7,8.4,8.4"></path>
						<path d="M136.7,122.1L120.3,122.1"></path>
					</g>
					<path d="M146.3,130.5L146.3,105"></path>
					<g>
						<path d="M154.2,122.2A8.4,8.4 0,1,1 171,122.2A8.4,8.4 0,1,1 154.2,122.2"></path>
						<path d="M171,130.5L171,105"></path>
					</g>
				</g>
			</svg>
		</div>
		<div class="lang trans-all-4">
			<ul>
				<li class="active">
					<a href="#" title="Georgian">Ge</a>
				</li>
				<li>
					<a href="#" title="English">En</a>
				</li>
			</ul>
		</div>
		<nav class="tree_menu trans-no-all">
			<ul>
				<li>
					<a href="#" title="მენიუ">
						<em data-feather="menu"></em>
						<span>მენიუ</span>
					</a>
				</li>
				<li>
					<a href="#" title="გვერდები">
						<em data-feather="file-text"></em>
						<span>გვერდები</span>
					</a>
				</li>
				<li>
					<a href="#" title="ადმინისტრაცია">
						<em data-feather="user"></em>
						<span>ადმინისტრაცია</span>
					</a>
					<ul>
						<li><a href="#" title="მომხმარებლები">მომხმარებლები</a></li>
						<li><a href="#" title="ჯგუფები">ჯგუფები</a>
					</ul>
				</li>
				<li>
					<a href="#" title="სლაიდერი">
						<em data-feather="sidebar"></em>
						<span>სლაიდერი</span>
					</a>
				</li>
				<li>
					<a href="#" title="ბანერები">
						<em data-feather="server"></em>
						<span>ბანერები</span>
					</a>
				</li>
				<li class="active">
					<a href="#" title="მედია">
						<em data-feather="image"></em>
						<span>მედია</span>
					</a>
				</li>
				<li>
					<a href="#" title="პარტნიორები">
						<em data-feather="users"></em>
						<span>პარტნიორები</span>
					</a>
				</li>
				<li>
					<a href="#" title="პარამეტრები">
						<em data-feather="settings"></em>
						<span>პარამეტრები</span>
					</a>
					<ul>
						<li><a href="#" title="ინფორმაციული ველები">ინფორმაციული ველები</a></li>
						<li><a href="#" title="ლოგირება">ლოგირება</a>
						<li><a href="#" title="ბაზა">ბაზა</a>
						<li><a href="#" title="სერვერი">სერვერი</a>
					</ul>
				</li>
				<li>
					<a href="#" title="თარგმნა">
						<em data-feather="flag"></em>
						<span>თარგმნა</span>
					</a>
				</li>
			</ul>
		</nav>
		<div class="artmedia">
			<span>product of</span>
			<a href="http://artmedia.ge" target="_blank" title="Artmedia">
				<svg class="nosvg">
					<g>
						<path d="M1.3,13.4c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5S1.3,16.4,1.3,13.4"></path>
						<path d="M12.3,7.9v11"></path>
						<path d="M18.3,7.9v11"></path>
						<path d="M18.3,11.9c0-2.2,1.8-4,4-4s4,1.8,4,4"></path>
						<path d="M36.3,18.9c-2.2,0-4-1.8-4-4v-14"></path>
						<path d="M35.3,7.9h-6"></path>
						<path d="M40.3,7.9v11"></path>
						<path d="M48.3,11.9c0-2.2,1.8-4,4-4s4,1.8,4,4v7"></path>
						<path d="M40.3,11.9c0-2.2,1.8-4,4-4s4,1.8,4,4v5"></path>
						<path d="M69.3,17.6c-1,0.8-2.2,1.3-3.5,1.3c-3,0-5.5-2.5-5.5-5.5s2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5h-11"></path>
						<path d="M75.1,13.4c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5S75.1,16.4,75.1,13.4"></path>
						<path d="M86.1,0.9v18"></path>
						<path d="M90.9,7.9v11"></path>
						<path d="M90.9,0.9v2"></path>
						<path d="M95.5,13.4c0-3,2.5-5.5,5.5-5.5s5.5,2.5,5.5,5.5s-2.5,5.5-5.5,5.5S95.5,16.4,95.5,13.4"></path>
						<path d="M106.5,7.9v11"></path>
					</g>
				</svg>
			</a>
		</div>
	</aside>

	<div class="main_wrap trans-all-4">
		<header>
			<h1 class="title">
				<figure></figure>
				<span>მედია</span>
			</h1>
			<div class="header_tools">
				<ul>
					<li class="to_site">
						<a href="#" target="_blank" title="საიტზე გადასვლა">
							<em data-feather="link"></em>
						</a>
					</li>
					<li class="help">
						<a href="#" title="დახმარება">
							<em data-feather="help-circle"></em>
						</a>
					</li>
					<li class="logout">
						<a href="#" title="სისტემიდან გამოსვლა">
							<em data-feather="log-out"></em>
						</a>
					</li>
				</ul>
			</div>
		</header>
		<main class="widget medium">
			<section class="content_wrap">
				<form action="" autocomplete="off">
					<div class="media_manager trans-all-2">
						<div class="media_manager_left">
							<div class="form_upload" data-url="">
								<input class="file-input" data-url="" multiple accept=".pdf,.doc,.docx,.xls,.xlsx" name="file[]" type="file">
								<div class="form_upload_cloud">
									<div class="form_upload_icon">
										<em data-feather="upload"></em>
									</div>
									<div class="form_upload_texts">ატვირთეთ ფაილები<span>(ან ჩააგდეთ ამ გრაფაში)</span></div>
								</div>
								<div class="upload_progress"></div>
							</div>
							<div class="add_search">
								<div class="add">
									<a href="#" title="შეტანა">
										<em data-feather="plus"></em>
										<span>შეტანა</span>
									</a>
								</div>
								<div class="delete_search">
									<div class="delete trans-no">
										<a href="javascript:void(0);" title="ყველას წაშლა">
											<em data-feather="trash-2"></em>
										</a>
									</div>
									<div class="search">
										<form action="" autocomplete="off">
											<input type="search" placeholder="საძიებო სიტყვა">
											<button type="submit">
												<em data-feather="search"></em>
											</button>
										</form>
									</div>
									<div class="toggle_params">
										<a href="javascript:void(0);" title="პარამეტრები">
											<em data-feather="settings"></em>
										</a>
									</div>
								</div>
							</div>
							<div class="uploaded_files" id="uploaded_files_container">
								<ul class="selectable">
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal" data-modal-type="edit_file_form" data-modal-title="ფაილის რედაქტირება" data-modal-id="476">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete call_modal" data-modal-type="delete_prompt" data-modal-title="გსურთ ფაილის წაშლა?" data-modal-id="476">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/img1.png" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/img1.png">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/img2.png" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/img2.png">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/img3.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/img3.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/img4.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/img4.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
									<li class="widget light">
										<figure>
											<img src="themes/images/no_image.jpg" alt="">
											<em data-feather="check"></em>
										</figure>
										<div class="link_edit_delete">
											<a href="#" title="ბმულის კოპირება" class="link">
												<em data-feather="link"></em>
												<input type="text" value="themes/images/no_image.jpg">
											</a>
											<a href="javascript:void(0);" title="რედაქტირება" class="edit call_modal">
												<em data-feather="edit"></em>
											</a>
											<a href="#" title="წაშლა" class="delete">
												<em data-feather="trash-2"></em>
											</a>
										</div>
										<div class="uploaded_title_wrap" title="Image title text goes here">Image title text ... .jpg</div>
									</li>
								</ul>
							</div>
							<div class="pagination">
								<ul>
									<li><a href="#" title="პირველი"><em data-feather="chevrons-left"></em></a></li>
									<li><a href="#" title="წინა"><em data-feather="chevron-left"></em></a></li>
									<li class="active"><a href="#" title="გვერდი 1">1</a></li>
									<li><a href="#" title="გვერდი 2">2</a></li>
									<li><a href="#" title="გვერდი 3">3</a></li>
									<li><a href="#" title="გვერდი 4">4</a></li>
									<li><a href="#" title="გვერდი 5">5</a></li>
									<li><a href="#" title="...">...</a></li>
									<li><a href="#" title="გვერდი 94">94</a></li>
									<li><a href="#" title="შემდეგი"><em data-feather="chevron-right"></em></a></li>
									<li><a href="#" title="ბოლო"><em data-feather="chevrons-right"></em></a></li>
								</ul>
								<div class="mobile">
									<a href="#" title="წინა" class="resp_prev"><em data-feather="arrow-left"></em></a>
									<a href="#" title="შემდეგი" class="resp_next"><em data-feather="arrow-right"></em></a>
								</div>
							</div>
						</div>
						<div class="media_manager_right">
							<div class="close">
								<a href="javascript:void(0);" title="დახურვა">
									<em data-feather="x"></em>
								</a>
							</div>
							<div class="sort">
								<select class="selectori" data-placeholder="ფაილის ტიპი">
									<option></option>
									<option>სურათები</option>
									<option>დოკუმენტები</option>
									<option>ვიდეოები</option>
								</select>
								<select class="selectori" data-placeholder="დრო">
									<option></option>
									<option>ოქტომბერი</option>
									<option>სექტემბერი</option>
									<option>ნოემბერი</option>
								</select>
							</div>
							<div class="size_watermark">
								<div class="size">
									<select class="selectori" data-placeholder="ზომა">
										<option></option>
										<option>1400x1050</option>
										<option>800x600</option>
										<option>400x300</option>
									</select>
								</div>
								<div class="watermark">
									<input type="checkbox" id="watermark" class="artform">
									<label for="watermark">Watermark</label>
								</div>
							</div>
							<div class="folders widget light">
								<ul>
									<li class="active">
										<a href="#" title="ყველა" class="folder_link">
											<em data-feather="folder"></em>
											<span>ყველა</span>
										</a>
										<a href="#" title="წაშლა" class="folder_delete"><em data-feather="trash-2"></em></a>
									</li>
									<li>
										<a href="#" title="სურათები" class="folder_link">
											<em data-feather="folder"></em>
											<span>სურათები</span>
										</a>
										<a href="#" title="წაშლა" class="folder_delete"><em data-feather="trash-2"></em></a>
										<ul>
											<li>
												<a href="#" title="ქვე დირექტორია" class="folder_link">
													<em data-feather="folder"></em>
													<span>ქვე დირექტორია</span>
												</a>
												<a href="#" title="წაშლა" class="folder_delete"><em data-feather="trash-2"></em></a>
											</li>
											<li>
												<a href="#" title="ქვე დირექტორია" class="folder_link">
													<em data-feather="folder"></em>
													<span>ქვე დირექტორია</span>
												</a>
												<a href="#" title="წაშლა" class="folder_delete"><em data-feather="trash-2"></em></a>
											</li>
										</ul>
									</li>
									<li>
										<a href="#" title="სურათები" class="folder_link">
											<em data-feather="folder"></em>
											<span>სურათები</span>
										</a>
										<a href="#" title="წაშლა" class="folder_delete"><em data-feather="trash-2"></em></a>
										<ul>
											<li>
												<a href="#" title="ქვე დირექტორია" class="folder_link">
													<em data-feather="folder"></em>
													<span>ქვე დირექტორია</span>
												</a>
												<a href="#" title="წაშლა" class="folder_delete"><em data-feather="trash-2"></em></a>
											</li>
											<li>
												<a href="#" title="ქვე დირექტორია" class="folder_link">
													<em data-feather="folder"></em>
													<span>ქვე დირექტორია</span>
												</a>
												<a href="#" title="წაშლა" class="folder_delete"><em data-feather="trash-2"></em></a>
											</li>
										</ul>
									</li>
								</ul>
							</div>
							<div class="new_folder widget light">
								<input type="text" placeholder="ფოლდერის სახელი">
								<select class="selectori" data-placeholder="მშობელი">
									<option></option>
									<option>სურათები</option>
									<option>ფაილები</option>
									<option>ვიდეოები</option>
								</select>
								<button type="submit" class="gilaki">დამატება</button>
							</div>
						</div>
					</div>
				</form>
			</section>
		</main>
	</div>
</div>

</body>
</html>